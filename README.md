# A Stopwatch App

A distribution of [this app is online here](http://stopwatch.treenomony.com)

![Desktop display (Google Chrome)](assets/images/mobile.png "Mobile Screenshot -- Google Chrome/ Android")

## Features

- Stores up to 999 (!) lap times!
- Persists lap times!
- Live split time!
- **Hot**-reset!

## Details

- Built as a VueJS single-file component: [`src/components/Stopwatch.vue`](`src/components/Stopwatch.vue`)
- Developed using the [_Vite_ build tool](https://github.com/vitejs/vite)

## Usage 

- To use locally, run `npm install` and `npm run dev` from this directory, then [open this url](http://localhost:3000)
- Otherwise, visit [the distribution installed here](http://stopwatch.treenomony.com)
  
